#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <fuse.h>
#include <dirent.h>

static const char *dirpath = "/home/csn/Documents";
char prefix[10] = "Animeku_";
char prefix2[10] = "IAN_";
char prefix3[12] = "nam_do-saq_";
char key[15] = "INNUGANTENG";

void enkripsi1(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;

  char a[10];
  char new[1000];
  int totalN = 0, flag = 0, ext = 0;
  memset(a, 0, sizeof(a));
  memset(new, 0, sizeof(new));

  for (int i = 0; i < strlen(string); i++)
  {
    if (string[i] == '/')
      continue;
    if (string[i] == '.')
    {
      new[totalN++] = string[i];
      flag = 1;
    }
    if (flag == 1)
      a[ext++] = string[i];
    else
      new[totalN++] = string[i];
  }

  for (int i = 0; i < totalN; i++)
  {
    if (isupper(new[i]))
      new[i] = 'A' + 'Z' - new[i];
    else if (islower(new[i]))
    {
      if (new[i] > 109)
        new[i] -= 13;
      else
        new[i] += 13;
    }
  }

  strcat(new, a);
  strcpy(string, new);
}

void logged(char *command, char *msg, char *old, char *new)
{
  FILE *fptr;
  fptr = fopen("Wibu.log", "a");
  if (fptr == NULL)
  {
    printf("[Error] : [Tidak dapat membuka file]");
    exit(1);
  }
  fprintf(fptr, "%s\t%s\t%s\t-->\t%s\n", command, msg, old, new);
  fclose(fptr);
}
