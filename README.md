# :zap: **soal-shift-sisop-modul-4-ITA10-2022** :zap:

| Nama                      | NRP        |
|---------------------------|------------|
| Muhammad Firdho Kustiawan | 5027201005 |
| Sharira Saniane           | 5027201016 |
| Jovan Surya Bako          | 5027201013 | 
<br/>


## :large_blue_circle: **Soal 1** :large_blue_circle: 
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:
	
a. Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
	
	Contoh : 
	“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”

b. Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

c. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

d. Setiap data yang terencode akan masuk dalam file “Wibu.log” 

Contoh isi: 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba

e. Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

Note : filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root) filesystem adalah directory /home/[USER]/Documents, dalam penamaan file ‘/’ diabaikan, dan ekstensi tidak perlu di-encode

### a. Encode File Dengan Atbash Cipher dan Rot13
   setiap direktori yang diawali “Animeku_” akan terencode dengan ketentuan semua file yang terdapat **huruf besar** akan ter encode dengan **atbash cipher** dan **huruf kecil** akan terencode dengan **rot13**. dengan menggunakan beberapa langkah sebagai berikut : 

- Function **enkripsi1(char*string)** akan diperiksa terlebih dahulu apakah `file` berisi `.` atau `..`, jika merupakan salah satu dari pilihan tersebut, maka encode tidak perlu dilakukan.
-  Selanjutnya, dibuat variabel `a` untuk menampung extension suatu file dan `new` untuk menampung nama file atau folder yang akan diencode(tanpa extension) yang masing-masing variabel bertipe string. Masing-masing variabel akan dilakukan `memset` yang bertujuan mengisi setiap char pada string supaya tidak menghasilkan karakter aneh pada output. Variabel lain yang dibuat yaitu `ext` yang akan memiliki panjang dari extension, `totalN` yang akan memiliki panjang dari nama file, dan `flag` yang digunakan sebagai penanda antara nama file dan extensionnya yang semua variabel tersebut diset memiliki nilai 0.
- Selanjutnya, akan dicari nama file atau folder beserta extensionnya jika ada yaitu dengan melakukan perulangan sebanyak `strlen(string)` atau panjang dari file. Pada perulangan, jika `string[i]` merupakan `.`, maka pada `new` akan ditambahkan `.` dan `flag` akan berubah nilainya menjadi 1 yang berarti sudah masuk pada fase extension. Namun, jika `string[i]` bukan merupakan `.`, maka akan diperiksa terlebih dahulu `flag` bernilai 0 atau 1, jika bernilai 0, maka `string[i]` akan ditambahkan pada `new`, jika sebaliknya, maka `string[i]` akan ditambahkan pada `a`. `totalN` akan bertambah ketika `new` bertambah dan `ext` akan bertambah ketika `a` bertambah.
- Setelah itu, `new` akan diencode dengan perulangan sebanyak `totalN`. Pada encode yang dilakukan, akan diperiksa apakah `new[i]` adalah **uppercase** atau **lowercase**. Jika merupakan **uppercase**, maka yang akan dilakukan adalah encode dengan atbash cipher yaitu menggunakan formula `'A' +'z' - new[i]`, tetapi jika merupakan **lowercase**, maka yang akan dilakukan adalah encode dengan rot13 yaitu penambahan 13 atau pengurangan 13. Pada encode dengan rot13, jika `newFile` merupakan karakter `a` hingga `m` maka akan dilakukan penambahan 13, tetapi jika `new` merupakan karakter `n` hingga `z` maka akan dilakukan pengurangan 13.
<div align="center">
    ![encode](https://i.ibb.co/rvJfPcD/encode.png)
</div>

- Lalu, `new` dan `a` akan digabungkan menggunakan `strcat()` dan hasilnya akan dicopy pada `string` menggunakan `strcpy()`.

Berikut merupakan code dari penjelasan diatas : 
```
void enkripsi1(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;

  char a[10];
  char new[1000];
  int totalN = 0, flag = 0, ext = 0;
  memset(a, 0, sizeof(a));
  memset(new, 0, sizeof(new));

  for (int i = 0; i < strlen(string); i++)
  {
    if (string[i] == '/')
      continue;
    if (string[i] == '.')
    {
      new[totalN++] = string[i];
      flag = 1;
    }
    if (flag == 1)
      a[ext++] = string[i];
    else
      new[totalN++] = string[i];
  }

  for (int i = 0; i < totalN; i++)
  {
    if (isupper(new[i]))
      new[i] = 'A' + 'Z' - new[i];
    else if (islower(new[i]))
    {
      if (new[i] > 109)
        new[i] -= 13;
      else
        new[i] += 13;
    }
  }

  strcat(new, a);
  strcpy(string, new);
}
```

### b,c. Rename File lalu Encode atau Decode
  Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

-  Untuk dapat menyelesaikan permasalahan pada no 1 dibutuhkan operasi getattr, readdir, read, dan operasi rename.
- pertama kita lakukan adalah mencari tau apakah nama file tersebut mengandung prefix yang diminta dalam hal soal no 1, prefix yang diminta adalah "Animeku_", maka dapat digunakan method stl pada string dengan `strstr(path, prefix)` dimana variabel `prefix` merupakan array yang berisi "Animeku_". 
- Lalu dicek apakah variabel `str` berisi atau tidak, jika tidak berisi maka artinya tidak ditemukan file dengan awalan "Animeku_" sedangkan jika bertemu maka diartikan bahwa ditemukan file yang berawalan "Animeku_".
- Lalu kita mau memindahkan kursor address dari index ke - 0 menjadi nama file atau folder pertama setelah prefix folder, maka kita akan menambahkan kursor tersebut sebanyak panjang prefix yang diminta lalu tambahkan method stl `strchr(str,'/')` yang artinya itu menemukan folder setelah folder dengan awalan prefix.
- Lalu sisa dari string tersebut akan di dienkripsikan atau didecode menggunakan function `enkripsi1(temp)` hal ini dilakukan karena decode dan encode untuk no 1 sama persis.

Berikut merupakan code dari penjelasan diatas : 

```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};
```

```
static int xmp_getattr(const char *path, struct stat *stbuf)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = lstat(fpath, stbuf);
  if (res == -1)
    return -errno;

  return 0;
}
```

```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  DIR *dp;
  struct dirent *de;
  (void)offset;
  (void)fi;
  dp = opendir(fpath);

  if (dp == NULL)
    return -errno;

  while ((de = readdir(dp)) != NULL)
  {
    if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
      continue;
    struct stat st;
    memset(&st, 0, sizeof(st));
    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    if (str != NULL)
      enkripsi1(de->d_name);
    if (str2 != NULL)
      enskripsi2(de->d_name);
    if (str3 != NULL)
      getSpecialFile(de->d_name);

    res = (filler(buf, de->d_name, &st, 0));
    if (res != 0)
      break;
  }

  logged2("READDIR", fpath, "");

  closedir(dp);
  return 0;
}
```

```
static int xmp_rename(const char *source, const char *destination)
{
  int res;
  char fpath_source[1000];
  char fpath_destination[1000];
  char *p_fpath_source, *p_fpath_destination;

  if (strcmp(source, "/") == 0)
  {
    source = dirpath;
    sprintf(fpath_source, "%s", source);
  }
  else
    sprintf(fpath_source, "%s%s", dirpath, source);

  if (strcmp(source, "/") == 0)
    sprintf(fpath_destination, "%s", dirpath);
  else
    sprintf(fpath_destination, "%s%s", dirpath, destination);

  res = rename(fpath_source, fpath_destination);
  if (res == -1)
    return -errno;

  p_fpath_source = strrchr(fpath_source, '/');
  p_fpath_destination = strrchr(fpath_destination, '/');
  if (strstr(p_fpath_source, "Animeku_"))
    logged("RENAME", "terdecode", fpath_source, fpath_destination);
  if (strstr(p_fpath_destination, "Animeku_"))
    logged("RENAME", "terenkripsi", fpath_source, fpath_destination);

  logged2("RENAME", fpath_source, fpath_destination);

  return 0;
}
```

  

### D. Menulis Log Rename pada "Wibu.log"
  Setiap data yang terencode akan masuk dalam file **“Wibu.log”**

  1. pertama kita lakukan adalah membuka file log tersebut yaitu "Wibu.log" dengan mode "a" yaitu *append* .
  2. Lalu dicek apakah file berhasil dibuka atau tidak, jika tidak maka outputkan errorrnya dan melakukan `exit(1)`.
  3. Jika berhasil maka printf kan ke dalam file tersebut sesuai dengan format yang ada dalam soal ke-1.
  4. Tutup file jika sudah tidak digunakkan lagi.
  5. Pencatatan log akan dilakukan pada rename yaitu di method `xmp_rename` dengan melakukan pengecekan pada variabel `p_fpath_source` atau `p_fpath_destination` apakah mengandung kata prefix yang diminta, jika `p_fpath_source` mengandung kata prefix yang diminta artinya file tersebut akan di decode sedangkan jika `p_fpath_destination` yang tidak kosong alias terdapat prefix yang diminta artinya file tersebut akan di enkripsikan.
  
  Berikut merupakan code dari penjelasan diatas : 
```
void logged(char *command, char *msg, char *old, char *new)
{
  FILE *fptr;
  fptr = fopen("Wibu.log", "a");
  if (fptr == NULL)
  {
    printf("[Error] : [Gagal dalam membuka file]");
    exit(1);
  }
  fprintf(fptr, "%s\t%s\t%s\t-->\t%s\n", command, msg, old, new);
  fclose(fptr);
}
```








